﻿using oneKeyDeployment.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oneKeyDeployment.App
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                PublishWebHelper.Execute();
                Console.WriteLine("执行成功");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
    }
}
