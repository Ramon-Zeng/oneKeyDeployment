﻿using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oneKeyDeployment.Model
{
    public class IISConfig
    {
        /// <summary>
        /// 网站名称
        /// </summary>
        public string WebName { get; set; }
        /// <summary>
        /// 端口
        /// </summary>
        public int Port { get; set; }
        /// <summary>
        /// 服务器域名
        /// </summary>
        public string ServerDomainName { get; set; }
        /// <summary>
        /// 网站目录
        /// </summary>
        public string WebsiteDirectory { get; set; }
        /// <summary>
        /// 程序池名称
        /// </summary>
        public string ApplicationPoolName { get; set; }
        /// <summary>
        /// 虚拟目录
        /// </summary>
        public VDir[] VDir { get; set; }
        /// <summary>
        /// 子应用程序
        /// </summary>
        public Application[] Applications { get; set; }
        /// <summary>
        /// 程序池
        /// </summary>
        public ApplicationPool[] ApplicationPools { get; set; }
    }

    /// <summary>
    /// 虚拟目录
    /// </summary>
    public class VDir {
        /// <summary>
        /// 虚拟目录名称
        /// </summary>
        public string DirName { get; set; }
        /// <summary>
        /// 物理路径
        /// </summary>
        public string PhysicalPath { get; set; }
    }


    /// <summary>
    /// 子应用程序
    /// </summary>
    public class Application
    {
        /// <summary>
        /// 路径名称
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// 物理路径
        /// </summary>
        public string PhysicalPath { get; set; }
        /// <summary>
        /// 应用程序池名称
        /// </summary>
        public string ApplicationPoolName { get; set; }
        /// <summary>
        /// 虚拟目录
        /// </summary>
        public VDir[] VDir { get; set; }
    }

    /// <summary>
    /// 应用程序池
    /// </summary>
    public class ApplicationPool
    {
        /// <summary>
        /// 版本编号 v4.0
        /// </summary>
        public string ManagedRuntimeVersion { get; set; }
        /// <summary>
        /// 队列长度
        /// </summary>
        public int QueueLength { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 启动模式
        /// </summary>
        public StartMode StartMode { get; set; }
        /// <summary>
        /// 启用32位应用程序
        /// </summary>
        public bool? Enable32BitAppOnWin64 { get; set; }
        /// <summary>
        /// 托管管道模式
        /// </summary>
        public ManagedPipelineMode ManagedPipelineMode { get; set; }
        /// <summary>
        /// CPU
        /// </summary>
        public Cpu Cpu { get; set; }
        /// <summary>
        /// 回收
        /// </summary>
        public Recycling Recycling { get; set; }
        /// <summary>
        /// 进程孤立
        /// </summary>
        public Failure Failure { get; set; }
        /// <summary>
        /// 进程模型
        /// </summary>
        public ProcessModel ProcessModel { get; set; }

    }

    /// <summary>
    /// CPU
    /// </summary>
    public class Cpu
    {
        /// <summary>
        /// Limit = 50000 限制最大CPU 50% , 默认为0
        /// </summary>
        public int Limit { get; set; }
        /// <summary>
        /// 限制操作
        /// </summary>
        public ProcessorAction Action { get; set; }
        /// <summary>
        /// 时间间隔（分钟）
        /// </summary>
        public int ResetInterval { get; set; }
        /// <summary>
        /// 已启用处理器关联
        /// </summary>
        public bool? SmpAffinitized { get; set; }
    }

    /// <summary>
    /// 回收
    /// </summary>
    public class Recycling
    {
        /// <summary>
        /// 发生配置更改时禁止回收
        /// </summary>
        public bool? DisallowRotationOnConfigChange { get; set; }
        /// <summary>
        /// 禁用重叠回收
        /// </summary>
        public bool? DisallowOverlappingRotation { get; set; }
        /// <summary>
        /// 生成和回收时间日志条目
        /// </summary>
        public RecyclingLogEventOnRecycle[] LogEventOnRecycle { get; set; }
        /// <summary>
        /// 定期重启
        /// </summary>
        public PeriodicRestart PeriodicRestart { get; set; }
    }
    /// <summary>
    /// 定期重启
    /// </summary>
    public class PeriodicRestart
    {
        /// <summary>
        /// 固定时间间隔(分钟)
        /// </summary>
        public int Time { get; set; }
        /// <summary>
        /// 请求限制 默认为零
        /// </summary>
        public int Requests { get; set; }
        /// <summary>
        /// 虚拟内存限制(KB)
        /// </summary>
        public int Memory { get; set; }
        /// <summary>
        /// 专用内存限制(KB)
        /// </summary>
        public int PrivateMemory { get; set; }
        /// <summary>
        /// 特定时间
        /// </summary>
        public TimeSpan[] Schedule { get; set; }
    }

    /// <summary>
    /// 进程孤立
    /// </summary>
    public class Failure
    {
        /// <summary>
        /// 可执行文件
        /// </summary>
        public string OrphanActionExe { get; set; }
        /// <summary>
        /// 可执行文件参数
        /// </summary>
        public string OrphanActionParams { get; set; }
        /// <summary>
        /// 已启用
        /// </summary>
        public bool? OrphanWorkerProcess { get; set; }
    }

    /// <summary>
    /// 进程模型
    /// </summary>
    public class ProcessModel
    {
        /// <summary>
        /// Ping间隔(秒)
        /// </summary>
        public int PingInterval { get; set; }
        /// <summary>
        /// Ping最大响应时间(秒)
        /// </summary>
        public int PingResponseTime { get; set; }
        /// <summary>
        /// 标识 0 LocalSystem , 1 LocalService , 2 NetworkService, 3 SpecificUser, 4 ApplicationPoolIdentity
        /// </summary>
        public ProcessModelIdentityType IdentityType { get; set; }
        /// <summary>
        /// 标识 账号
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 标识 密码
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 关闭时间限制(秒)
        /// </summary>
        public int ShutdownTimeLimit { get; set; }
        /// <summary>
        /// 加载用户配置文件
        /// </summary>
        public bool? LoadUserProfile { get; set; }
        /// <summary>
        /// 空闲超时操作 0 Terminate / 1 Suspend
        /// </summary>
        public int IdleTimeoutAction { get; set; }
        /// <summary>
        /// 启动时间限制(秒)
        /// </summary>
        public int StartupTimeLimit { get; set; }
        /// <summary>
        /// 启用 Ping
        /// </summary>
        public bool? PingingEnabled { get; set; }
        /// <summary>
        /// 生成进程模型事件日志条目-空闲超时已到
        /// </summary>
        public ProcessModelLogEventOnProcessModel LogEventOnProcessModel { get; set; }
        /// <summary>
        /// 闲置超时(分钟)
        /// </summary>
        public int IdleTimeout { get; set; }
        /// <summary>
        /// 最大工作进程数
        /// </summary>
        public int MaxProcesses { get; set; }
    }
}
